import { type OutgoingHttpHeaders } from 'node:http';
import { defineConfig } from 'vite';
import { createHtmlPlugin } from 'vite-plugin-html';
import elmPlugin from 'vite-plugin-elm';
import banner from 'vite-plugin-banner';

/** @see https://www.npmjs.com/package/z3-solver#limitations */
const coiHeaders: OutgoingHttpHeaders = {
  'Cross-Origin-Embedder-Policy': 'require-corp',
  'Cross-Origin-Resource-Policy': 'cross-origin',
  'Cross-Origin-Opener-Policy': 'same-origin'
};

export default defineConfig({
  define: {
    'import.meta.vitest': 'undefined',
    'global': 'globalThis',
  },
  plugins: [
    banner('Dernik Labs :: Exploring the Unknown'),
    elmPlugin(),
    createHtmlPlugin({
      minify: true
    })
  ],
  server: {
    headers: coiHeaders
  },
  preview: {
    headers: coiHeaders
  },
  test: {
    // reporters: ['verbose']
  }
});
