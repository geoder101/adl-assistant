import { describe, expect } from 'vitest';
import randomstring from 'randomstring';
import { sum } from '../../../src/ts-utils/array';
import { testZ3 } from '../testZ3';
import {
  calculateWorkload,
  type CalculateWorkloadReq,
  type CalculateWorkloadRes
} from '../../../src/oracle/sprint';

describe('calculateWorkload', () => {
  testZ3('exact: actual sprints are as many as estimated', async ({ z3 }) => {
    const $correlationId = randomstring.generate();
    const storyPoints = 13 * 5;
    const req = mkCalculateWorkloadReq($correlationId, storyPoints);
    const res = await calculateWorkload(z3, req);
    expectEqualResKindAndStoryPoints(res, $correlationId, 'exact', storyPoints);
  });

  testZ3('underflow: actual sprints are less than estimated', async ({ z3 }) => {
    const $correlationId = randomstring.generate();
    const storyPoints = 13 * 5 - 13;
    const req = mkCalculateWorkloadReq($correlationId, storyPoints);
    const res = await calculateWorkload(z3, req);
    expectEqualResKindAndStoryPoints(res, $correlationId, 'underflow', storyPoints);
  });

  testZ3('overflow: actual sprints are more than estimated', async ({ z3 }) => {
    const $correlationId = randomstring.generate();
    const storyPoints = 13 * 5 + 13;
    const req = mkCalculateWorkloadReq($correlationId, storyPoints);
    const res = await calculateWorkload(z3, req);
    expectEqualResKindAndStoryPoints(res, $correlationId, 'overflow', storyPoints);
  });

  testZ3('inconclusive: cannot decide what is what due to narrow view', async ({ z3 }) => {
    const $correlationId = randomstring.generate();
    const storyPoints = 13 * 2;
    const req: CalculateWorkloadReq = {
      $correlationId,
      requestedSprints: 1,
      requestedStoryPoints: storyPoints,
      daysPerSprint: 1,
      sprintUpperTolerance: 0,
      constCapacityPerSprint: 1
    };
    const res = await calculateWorkload(z3, req);
    expect(res.kind).toBe('inconclusive');
    expect(res.$correlationId).toBe($correlationId);
  });
});

function mkCalculateWorkloadReq($correlationId: string, storyPoints: number) {
  const req: CalculateWorkloadReq = {
    $correlationId,
    requestedSprints: 1,
    requestedStoryPoints: storyPoints,
    constCapacityPerSprint: 1
  };
  return req;
}

function expectEqualResKindAndStoryPoints(
  res: CalculateWorkloadRes,
  $correlationId: string,
  expectedResKind: Exclude<CalculateWorkloadRes['kind'], 'inconclusive'>,
  expectedStoryPoints: number) {
  expect(res.kind).toBe(expectedResKind);
  expect(res.$correlationId).toBe($correlationId);

  const actualStoryPoints =
    (res as CalculateWorkloadRes & { kind: typeof expectedResKind; }).storyPoints;

  expect(sum(actualStoryPoints)).toEqual(expectedStoryPoints);
}
