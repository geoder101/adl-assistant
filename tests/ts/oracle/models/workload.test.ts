import { describe, expect } from 'vitest';
import { sum } from '../../../../src/ts-utils/array';
import { testZ3 } from '../../testZ3';
import { solve, type Z3 } from '../../../../src/oracle/solver';
import {
  constNumOfSubTasksPerTask,
  WorkloadModel,
  type WorkloadModelConfiguration
} from '../../../../src/oracle/models/workload';

describe('Workload Model', () => {
  testZ3('total story points of a period is the aggregation of its tasks', async ({ z3 }) => {
    const expectedStoryPoints = 2;
    const modelConfig: WorkloadModelConfiguration = {
      totalStoryPoints: expectedStoryPoints,
      numOfNormalPeriods: 1,
      numOfExtraPeriods: 0,
      numOfTasksPerPeriod: 2,
      numOfSubTasksPerTask: constNumOfSubTasksPerTask(1)
    };
    const actualStoryPoints = await solveStoryPoints(z3, modelConfig);
    expect(actualStoryPoints).toEqual(expectedStoryPoints);
  });

  testZ3('total story points of a task is the aggregation of its subtasks', async ({ z3 }) => {
    const expectedStoryPoints = 2;
    const modelConfig: WorkloadModelConfiguration = {
      totalStoryPoints: expectedStoryPoints,
      numOfNormalPeriods: 1,
      numOfExtraPeriods: 0,
      numOfTasksPerPeriod: 1,
      numOfSubTasksPerTask: constNumOfSubTasksPerTask(2)
    };
    const actualStoryPoints = await solveStoryPoints(z3, modelConfig);
    expect(actualStoryPoints).toEqual(expectedStoryPoints);
  });

  describe.each([
    {
      case: 'a combination of valid story points',
      numOfTasks: 2,
      totalStoryPoints: 3,
      availableStoryPoints: [1, 2, 3],
      expectedStoryPoints: [1, 2]
    },
    {
      case: 'a single valid story point',
      numOfTasks: 1,
      totalStoryPoints: 3,
      availableStoryPoints: [1, 2, 3],
      expectedStoryPoints: [3]
    },
    {
      case: 'an invalid set of story points',
      numOfTasks: 1,
      totalStoryPoints: 4,
      availableStoryPoints: [1, 2, 3],
      expectedStoryPoints: undefined
    }
  ])('Given $case', ({ numOfTasks, totalStoryPoints, availableStoryPoints, expectedStoryPoints }) => {
    testZ3('story points of a subtask are selected from a specific finite set of options', async ({ z3 }) => {
      const modelConfig: WorkloadModelConfiguration = {
        totalStoryPoints,
        numOfNormalPeriods: 1,
        numOfExtraPeriods: 0,
        numOfTasksPerPeriod: numOfTasks,
        numOfSubTasksPerTask: constNumOfSubTasksPerTask(1),
        availableStoryPoints
      };
      const model = new WorkloadModel(z3, modelConfig);
      const actualStoryPoints = await solve(z3, model);
      expect(actualStoryPoints?.sort()).toEqual(expectedStoryPoints?.sort());
    });
  });

  testZ3('normal period total story points must equal the theoretical capacity and is sat', async ({ z3 }) => {
    const modelConfig: WorkloadModelConfiguration = {
      totalStoryPoints: 4,
      numOfNormalPeriods: 2,
      numOfExtraPeriods: 0,
      numOfTasksPerPeriod: 2,
      numOfSubTasksPerTask: constNumOfSubTasksPerTask(1),
      availableStoryPoints: [0, 1]
    };
    const expectedStoryPoints = [1, 1, 1, 1];
    const model = new WorkloadModel(z3, modelConfig);
    const actualStoryPoints = await solve(z3, model);
    expect(actualStoryPoints).toEqual(expectedStoryPoints);
  });

  testZ3('normal period total story points must equal the theoretical capacity and is unsat', async ({ z3 }) => {
    const modelConfig: WorkloadModelConfiguration = {
      totalStoryPoints: 3,
      numOfNormalPeriods: 2,
      numOfExtraPeriods: 0,
      numOfTasksPerPeriod: 2,
      numOfSubTasksPerTask: constNumOfSubTasksPerTask(1),
      availableStoryPoints: [0, 1]
    };
    const model = new WorkloadModel(z3, modelConfig);
    const actualStoryPoints = await solve(z3, model);
    expect(actualStoryPoints).toBeUndefined();
  });

  testZ3('extra period total story points may be less than the theoretical capacity', async ({ z3 }) => {
    const modelConfig: WorkloadModelConfiguration = {
      totalStoryPoints: 3,
      numOfNormalPeriods: 1,
      numOfExtraPeriods: 1,
      numOfTasksPerPeriod: 2,
      numOfSubTasksPerTask: constNumOfSubTasksPerTask(1),
      availableStoryPoints: [0, 1]
    };
    const expectedStoryPoints = [1, 1, 1];
    const model = new WorkloadModel(z3, modelConfig);
    const actualStoryPoints = await solve(z3, model);
    expect(actualStoryPoints).toEqual(expectedStoryPoints);
  });

  testZ3('extra period may be the only period', async ({ z3 }) => {
    const modelConfig: WorkloadModelConfiguration = {
      totalStoryPoints: 2,
      numOfNormalPeriods: 0,
      numOfExtraPeriods: 1,
      numOfTasksPerPeriod: 3,
      numOfSubTasksPerTask: constNumOfSubTasksPerTask(1),
      availableStoryPoints: [0, 1]
    };
    const expectedStoryPoints = [1, 1];
    const model = new WorkloadModel(z3, modelConfig);
    const actualStoryPoints = await solve(z3, model);
    expect(actualStoryPoints).toEqual(expectedStoryPoints);
  });
});

async function solveStoryPoints(z3: Z3, modelConfig: WorkloadModelConfiguration): Promise<number | undefined> {
  const model = new WorkloadModel(z3, modelConfig);
  const storyPoints = await solve(z3, model);
  if (storyPoints === undefined) {
    return undefined;
  }
  const storyPointsSum = sum(storyPoints);
  return storyPointsSum;
}
