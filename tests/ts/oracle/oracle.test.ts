import { Result } from '@badrap/result';
import { describe, expect } from 'vitest';
import { testZ3 } from '../testZ3';
import {
  type Z3,
  type Arith,
  type OracleEvaluatedModel,
  OracleOutcomeError,
  OracleModel,
  trySolve,
  solve,
} from '../../../src/oracle/solver';

describe('satisfiable model', () => {
  describe('trySolve', () => {
    testZ3('single evaluation resolves to an answer', async ({ z3 }) => {
      const model = new SatSumModel(z3, 1, 2);
      const answer = await trySolve(z3, model);
      expect(answer).toEqual(Result.ok({ x: 1, y: 2, z: 3 }));
    });

    testZ3('repeated evaluations resolve to the same answer', async ({ z3 }) => {
      const model = new SatSumModel(z3, 1, 2);
      const answer1 = await trySolve(z3, model);
      const answer2 = await trySolve(z3, model);
      expect(answer1).toEqual(answer2);
      expect(answer2).toEqual(Result.ok({ x: 1, y: 2, z: 3 }));
    });
  });

  describe('solve', () => {
    testZ3('single evaluation resolves to an answer', async ({ z3 }) => {
      const model = new SatSumModel(z3, 1, 2);
      const answer = await solve(z3, model);
      expect(answer).toEqual({ x: 1, y: 2, z: 3 });
    });

    testZ3('repeated evaluations resolve to the same answer', async ({ z3 }) => {
      const model = new SatSumModel(z3, 1, 2);
      const answer1 = await solve(z3, model);
      const answer2 = await solve(z3, model);
      expect(answer1).toEqual(answer2);
      expect(answer2).toEqual({ x: 1, y: 2, z: 3 });
    });
  });
});

describe('unsatisfiable model', () => {
  describe('trySolve', () => {
    testZ3("returns 'unsat' error", async ({ z3 }) => {
      const model = new UnsatSumModel(z3, 1, 2);
      const answer = await trySolve(z3, model);
      expect(answer).toEqual(Result.err(new OracleOutcomeError('unsat')));
    });
  });

  describe('solve', () => {
    testZ3('returns undefined', async ({ z3 }) => {
      const model = new UnsatSumModel(z3, 1, 2);
      const answer = await solve(z3, model);
      expect(answer).toBeUndefined();
    });
  });
});

class SatSumModel extends OracleModel<{ x: number, y: number; z: number; }> {
  private readonly modelX: NumberEqualsModel;
  private readonly modelY: NumberEqualsModel;
  protected readonly varZ: Arith;

  constructor(z3: Z3, protected readonly x: number, protected readonly y: number) {
    super(z3);
    this.modelX = new NumberEqualsModel(this.z3, z3.Int.fresh(), this.x);
    this.modelY = new NumberEqualsModel(this.z3, z3.Int.fresh(), this.y);
    this.varZ = this.z3.Int.fresh();
  }

  protected override realize(): void {
    this.attach(this.modelX, this.modelY);
    this.assert(this.varZ.eq(this.modelX.variable.add(this.modelY.variable)));
  }

  public override resolve(model: OracleEvaluatedModel): Result<{ x: number; y: number; z: number; }> {
    const x = this.modelX.resolve(model).unwrap();
    const y = this.modelY.resolve(model).unwrap();
    const z = this.eval.int(model, this.varZ).unwrap();
    return Result.ok({ x, y, z });
  }
}

class UnsatSumModel extends SatSumModel {
  constructor(z3: Z3, x: number, y: number) {
    super(z3, x, y);
  }

  protected override realize(): void {
    super.realize();
    this.assert(this.varZ.eq(-this.x - this.y));
  }
}

class NumberEqualsModel extends OracleModel<number> {
  constructor(z3: Z3, readonly variable: Arith, readonly value: number) {
    super(z3);
  }

  protected override realize(): void {
    this.assert(this.variable.eq(this.value));
  }

  public override resolve(model: OracleEvaluatedModel): Result<number> {
    return this.eval.int(model, this.variable);
  }
}
