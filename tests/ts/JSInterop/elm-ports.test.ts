import { type Mock, beforeEach, afterEach, describe, it, expect, vi } from 'vitest';
import type { ElmPortInProp, ElmPortOutProp } from '../../../src/ElmInterop/elm';
import { pipe } from '../../../src/ElmInterop/elm-ports';

describe('pipe', () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });

  afterEach(() => {
    vi.restoreAllMocks();
  });

  it('does not subscribe on undefined input port', async () => {
    const { portInFnMocks } = mkPortInMock();
    pipe(undefined, undefined, () => { });
    await vi.runAllTimersAsync();
    expect(portInFnMocks.subscribeFn).toHaveBeenCalledTimes(0);
  });

  it('subscribes on input port', async () => {
    const { portIn, portInFnMocks } = mkPortInMock();
    pipe(portIn, undefined, () => { });
    await vi.runAllTimersAsync();
    expect(portInFnMocks.subscribeFn).toHaveBeenCalledTimes(1);
  });

  it('reads from input port and calls subscription handler with default reader', async () => {
    const { portIn, portInFnMocks } = mkPortInMock();
    const handler = vi.fn<[string], string>();
    pipe(portIn, undefined, handler);
    await vi.runAllTimersAsync();
    expect(handler).toHaveBeenCalledTimes(0);
    portInFnMocks.subscribeTriggerFn('yo');
    await vi.runAllTimersAsync();
    expect(handler).toHaveBeenNthCalledWith(1, 'yo');
  });

  it('reads from input port and calls subscription handler with user-provided reader', async () => {
    const { portIn, portInFnMocks } = mkPortInMock();
    const handler = vi.fn<[string], string>();
    const reader = (x: unknown) => typeof x === 'string' ? x.toUpperCase() : '';
    pipe(portIn, undefined, handler, reader);
    await vi.runAllTimersAsync();
    expect(handler).toHaveBeenCalledTimes(0);
    portInFnMocks.subscribeTriggerFn('yo');
    await vi.runAllTimersAsync();
    expect(handler).toHaveBeenNthCalledWith(1, 'YO');
  });

  it('does not write to undefined output port', async () => {
    const { portIn, portInFnMocks } = mkPortInMock();
    const { portOutFnMocks } = mkPortOutMock();
    pipe(portIn, undefined, (x) => x);
    portInFnMocks.subscribeTriggerFn('yo');
    await vi.runAllTimersAsync();
    expect(portOutFnMocks.sendFn).toHaveBeenCalledTimes(0);
  });

  it('writes to output port', async () => {
    const { portIn, portInFnMocks } = mkPortInMock();
    const { portOut, portOutFnMocks } = mkPortOutMock();
    const handler = (x: string) => x.toUpperCase();
    pipe(portIn, portOut, handler);
    portInFnMocks.subscribeTriggerFn('yo');
    await vi.runAllTimersAsync();
    expect(portOutFnMocks.sendFn).toHaveBeenNthCalledWith(1, 'YO');
  });
});

type PortInFnMocks = {
  subscribeFn: Mock<[(msg: unknown) => void], void>,
  subscribeTriggerFn: Mock<[unknown], void>;
};

type PortOutFnMocks = {
  sendFn: Mock<[any], void>,
};

function mkPortInMock() {
  const portInFnMocks = mkPortInFnMocks();
  const portIn = mkPortInFromMockFns(portInFnMocks);
  return { portIn, portInFnMocks };
}

function mkPortOutMock() {
  const portOutFnMocks = mkPortOutFnMocks();
  const portOut = mkPortOutFromMockFns(portOutFnMocks);
  return { portOut, portOutFnMocks };
}

function mkPortInFromMockFns(portInMocks: PortInFnMocks): ElmPortInProp {
  return {
    subscribe: portInMocks.subscribeFn,
    unsubscribe: () => { }
  };
}

function mkPortOutFromMockFns(portOutMocks: PortOutFnMocks): ElmPortOutProp {
  return {
    send: portOutMocks.sendFn
  };
}

function mkPortInFnMocks(): PortInFnMocks {
  const subscribeFn = vi.fn<[(msg: unknown) => void], void>();
  const subscribeTriggerFn = vi.fn<[unknown], void>();

  subscribeFn.mockImplementationOnce(fn => {
    subscribeTriggerFn.mockImplementation(msg => {
      fn(msg);
    });
  });

  return { subscribeFn, subscribeTriggerFn };
}

function mkPortOutFnMocks(): PortOutFnMocks {
  const sendFn = vi.fn<[any], void>();
  return { sendFn };
}
