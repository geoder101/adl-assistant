import { test, expectTypeOf } from 'vitest';
import { type ThisElmInstance } from '../../../src/ElmInterop/app';

type ExpectedElmPortIn = {
  readonly subscribe: (handler: (msg: unknown) => void) => void,
  readonly unsubscribe: (handler: (msg: unknown) => void) => void;
};

type ExpectedElmPortOut = {
  readonly send: (msg: any) => void;
};

type ExpectedThisElmApp = {
  readonly ports: {
    readonly calculateStoryPoints: ExpectedElmPortIn,
    readonly calculateStoryPointsAnswer: ExpectedElmPortOut;
  };
};

type ExpectedElmInitArgs = {
  readonly node: Element | null,
  readonly flags?: any;
};

type ExpectedElmInitFn = (args: ExpectedElmInitArgs) => ExpectedThisElmApp;

type ExpectedThisElmInstance = {
  readonly Main: {
    readonly init: ExpectedElmInitFn;
  };
};

test('ThisElmInstance', () => {
  expectTypeOf<ThisElmInstance>().toMatchTypeOf<ExpectedThisElmInstance>();
});
