import { test } from 'vitest';
import { MkZ3, type Z3 } from '../../src/oracle/solver';

const z3 = await MkZ3();

export interface TestZ3Context {
  z3: Z3;
}

export const testZ3 = test.extend<TestZ3Context>({
  z3: async ({ }, use) => { await use(z3); }
});
