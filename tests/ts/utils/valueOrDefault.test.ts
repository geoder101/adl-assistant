import { expect, test } from 'vitest';
import valueOrDefault from '../../../src/ts-utils/valueOrDefault';

test.each([
  [undefined, -1, -1],
  [undefined, null, null],
  [null, -1, null],
  [5, -1, 5],
  [0, -1, 0]
])('valueOrDefault(%s, %s) = %s', (value, defaultValue, expectedValue) => {
  expect(valueOrDefault(value, defaultValue)).toBe(expectedValue);
});
