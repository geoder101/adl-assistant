import { expect, test } from 'vitest';
import { toArrayAsync, sum, zip } from '../../../src/ts-utils/array';

test('toArrayAsync', async () => {
  const asyncSequence = (async function* () {
    yield 1;
    yield 2;
    yield 3;
  })();
  var array = await toArrayAsync(asyncSequence);
  expect(array).toEqual([1, 2, 3]);
});

test('sum', () => {
  expect(sum([1, 3, 2])).toEqual(6);
});

test('zip', () => {
  const expectedPairs = [
    [1, 'a'],
    [2, 'b']
  ];

  expect(zip([1, 2], ['a', 'b'])).toEqual(expectedPairs);
  expect(zip([1, 2], ['a', 'b', 'c'])).toEqual(expectedPairs);
  expect(zip([1, 2, 3], ['a', 'b'])).toEqual(expectedPairs);
});
