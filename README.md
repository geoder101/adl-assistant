# ADL Assistant

> An experiment.

How can an ADL have a better overview of a project and be (more) confident of its progress?  
See inside for possible answers.

## How to run this app

1) After cloning, run `make init`

2) Run `make dev` and open the reported url (or just press `o`)

> **NOTE:** If you don't have `make` available in your system, then use `npm run <target>` (e.g. `npm run init` or `npm run dev`).
