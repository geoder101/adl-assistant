port module Main exposing (main)

import Browser exposing (Document)
import Components.BurndownChart as BurndownChart
import Element
import Element.PravdomilUi exposing (..)
import ElmInterop.Ports as Ports
import ElmInterop.StoryPoints as StoryPoints exposing (CalculateReq, CalculateRes(..))
import Json.Decode as D
import Json.Encode as E
import Platform.Cmd as Cmd
import Shell exposing (style, theme)



-- MAIN


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { burndownChartMain : BurndownChart.Model
    , burndownChartDemoExact : BurndownChart.Model
    , burndownChartDemoUnderflow : BurndownChart.Model
    , burndownChartDemoOverflow : BurndownChart.Model
    }


type ChartId
    = Main
    | DemoExact
    | DemoUnderflow
    | DemoOverflow


encodeChartId : ChartId -> String
encodeChartId chartId =
    case chartId of
        Main ->
            "main"

        DemoExact ->
            "demo:exact"

        DemoUnderflow ->
            "demo:underflow"

        DemoOverflow ->
            "demo:overflow"


decodeChartId : String -> Maybe ChartId
decodeChartId chartId =
    case chartId of
        "main" ->
            Just Main

        "demo:exact" ->
            Just DemoExact

        "demo:underflow" ->
            Just DemoUnderflow

        "demo:overflow" ->
            Just DemoOverflow

        _ ->
            Nothing



-- INIT


init : () -> ( Model, Cmd Msg )
init _ =
    let
        burndownChartMain =
            BurndownChart.init True
                { requestedSprints = 3
                , requestedStoryPoints = 0
                , capacityPerSprint = 2
                , calculatedStoryPoints = []
                }

        burndownChartDemoExact =
            BurndownChart.init False
                { requestedSprints = 3
                , requestedStoryPoints = 326
                , capacityPerSprint = 2
                , calculatedStoryPoints = [ 13, 26, 26, 13, 26, 1, 26, 26, 26, 26, 26, 26, 13, 26, 26 ]
                }

        burndownChartDemoUnderflow =
            BurndownChart.init False
                { requestedSprints = 3
                , requestedStoryPoints = 132
                , capacityPerSprint = 2
                , calculatedStoryPoints = [ 26, 14, 26, 13, 1, 13, 13, 26 ]
                }

        burndownChartDemoOverflow =
            BurndownChart.init False
                { requestedSprints = 3
                , requestedStoryPoints = 420
                , capacityPerSprint = 2
                , calculatedStoryPoints = [ 26, 26, 13, 13, 13, 14, 14, 26, 26, 26, 14, 26, 26, 26, 14, 26, 26, 26, 13, 26 ]
                }
    in
    ( Model
        burndownChartMain
        burndownChartDemoExact
        burndownChartDemoUnderflow
        burndownChartDemoOverflow
    , Cmd.none
    )



-- UPDATE


type Msg
    = JS Ports.Msg
    | UpdateBurndownChart ChartId BurndownChart.Msg
    | CalculateBurndownChartStoryPoints ChartId CalculateReq


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        burndownChartCalculatedStoryPointsError =
            Err "I'm sorry Dave, I'm afraid I cannot do that."
    in
    case msg of
        UpdateBurndownChart chartId burndownChartMsg ->
            let
                currBurndownChart =
                    model |> getBurndownChart chartId

                newBurndownChart =
                    currBurndownChart
                        |> BurndownChart.update burndownChartMsg

                newModel =
                    model |> setBurndownChart chartId newBurndownChart
            in
            ( newModel, Cmd.none )

        CalculateBurndownChartStoryPoints chartId req ->
            let
                encodedChartId =
                    encodeChartId chartId

                currBurndownChart =
                    model |> getBurndownChart chartId

                newBurndownChart =
                    currBurndownChart
                        |> BurndownChart.calculatingStoryPoints
                            { requestedSprints = req.requestedSprints
                            , requestedStoryPoints = req.requestedStoryPoints
                            , capacityPerSprint = req.capacityPerSprint
                            }

                newModel =
                    model |> setBurndownChart chartId newBurndownChart
            in
            update (JS << Ports.StoryPoints << StoryPoints.Calculate <| ( encodedChartId, req )) newModel

        JS (Ports.StoryPoints (StoryPoints.Calculate ( correlationId, req ))) ->
            ( model, calculateStoryPoints (StoryPoints.encodeReq ( correlationId, req )) )

        JS (Ports.StoryPoints (StoryPoints.CalculateAnswer (Ok ( encodedChartId, res )))) ->
            let
                chartId =
                    decodeChartId encodedChartId

                newModel =
                    case res of
                        Inconclusive ->
                            updateBurndownChartStoryPoints chartId burndownChartCalculatedStoryPointsError model

                        Exact storyPoints ->
                            updateBurndownChartStoryPoints chartId (Ok storyPoints) model

                        Underflow storyPoints ->
                            updateBurndownChartStoryPoints chartId (Ok storyPoints) model

                        Overflow storyPoints ->
                            updateBurndownChartStoryPoints chartId (Ok storyPoints) model
            in
            ( newModel, Cmd.none )

        JS (Ports.StoryPoints (StoryPoints.CalculateAnswer (Err _))) ->
            ( model, Cmd.none )


updateBurndownChartStoryPoints : Maybe ChartId -> Result String (List Int) -> Model -> Model
updateBurndownChartStoryPoints chartIdOption storyPoints model =
    case chartIdOption of
        Nothing ->
            model

        Just chartId ->
            let
                currBurndownChart =
                    model |> getBurndownChart chartId

                newBurndownChart =
                    currBurndownChart
                        |> BurndownChart.calculatedStoryPoints storyPoints

                newModel =
                    model |> setBurndownChart chartId newBurndownChart
            in
            newModel


getBurndownChart : ChartId -> Model -> BurndownChart.Model
getBurndownChart chartId model =
    case chartId of
        Main ->
            model.burndownChartMain

        DemoExact ->
            model.burndownChartDemoExact

        DemoUnderflow ->
            model.burndownChartDemoUnderflow

        DemoOverflow ->
            model.burndownChartDemoOverflow


setBurndownChart : ChartId -> BurndownChart.Model -> Model -> Model
setBurndownChart chartId burndownChart model =
    case chartId of
        Main ->
            { model | burndownChartMain = burndownChart }

        DemoExact ->
            { model | burndownChartDemoExact = burndownChart }

        DemoUnderflow ->
            { model | burndownChartDemoUnderflow = burndownChart }

        DemoOverflow ->
            { model | burndownChartDemoOverflow = burndownChart }



-- VIEW


view : Model -> Document Msg
view model =
    { title = "Dernik Labs :: ADL Assistant"
    , body =
        [ layout theme
            [--explain Debug.todo
            ]
            (viewBody model)
        ]
    }


viewBody : Model -> Element Msg
viewBody model =
    column [ width fill, height fill ]
        [ viewPane
            "ADL Assistant"
            [ lazy viewAssistant ( Main, model.burndownChartMain ) ]
        , viewPane
            "Demo: Exact"
            [ lazy viewAssistant ( DemoExact, model.burndownChartDemoExact ) ]
        , viewPane
            "Demo: Underflow"
            [ lazy viewAssistant ( DemoUnderflow, model.burndownChartDemoUnderflow ) ]
        , viewPane
            "Demo: Overflow"
            [ lazy viewAssistant ( DemoOverflow, model.burndownChartDemoOverflow ) ]
        ]


viewPane : String -> List (Element Msg) -> Element Msg
viewPane headerText content =
    column
        [ width (fill |> maximum 940)
        , centerX
        , padding 18
        ]
        [ column
            [ width fill
            , padding 8
            , borderWidth 1
            , borderRounded 8
            , borderColor style.primaryBack
            ]
            [ column
                [ width fill
                , spacing 12
                ]
                (el [ centerX ] (heading1 theme [] [ text headerText ])
                    :: content
                )
            ]
        ]


viewAssistant : ( ChartId, BurndownChart.Model ) -> Element Msg
viewAssistant ( chartId, burndownChartModel ) =
    BurndownChart.view burndownChartModel
        |> (Element.map << BurndownChart.onIntercept (UpdateBurndownChart chartId))
            (\(BurndownChart.CalculateStoryPoints params) ->
                let
                    req =
                        { requestedSprints = params.requestedSprints
                        , requestedStoryPoints = params.requestedStoryPoints
                        , capacityPerSprint = params.capacityPerSprint
                        }
                in
                CalculateBurndownChartStoryPoints chartId req
            )



-- PORTS


port calculateStoryPoints : E.Value -> Cmd msg


port calculateStoryPointsAnswer : (D.Value -> msg) -> Sub msg



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    calculateStoryPointsAnswer (JS << Ports.StoryPoints << StoryPoints.CalculateAnswer << StoryPoints.decodeRes)
