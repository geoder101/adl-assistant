export class ResultError<T> extends Error {
  constructor(readonly result: T, message?: string) {
    super(message);
  }
}
