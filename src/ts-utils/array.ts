// A simpler version,
// compared to <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/fromAsync>
// which is currently available only as a polyfill.
export async function toArrayAsync<T>(iter: AsyncIterable<T>): Promise<T[]> {
  const arr: T[] = [];
  for await (const item of iter) {
    arr.push(item);
  }
  return arr;
}

export function sum(arr: number[]) {
  return arr.reduce((a, b) => a + b, 0);
}

export function zip<A, B>(xs: A[], ys: B[]): [A, B][] {
  return xs.map((x, i) => <[A, B]>[x, ys[i]]).filter(p => p[1] !== undefined);
}
