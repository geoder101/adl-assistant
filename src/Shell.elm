module Shell exposing (..)

import Element.PravdomilUi exposing (..)
import Element.PravdomilUi.Theme exposing (Style, Theme)
import Element.PravdomilUi.Theme.Basic


style : Style
style =
    Element.PravdomilUi.Theme.Basic.style


theme : Theme msg
theme =
    Element.PravdomilUi.Theme.Basic.theme style
        |> (\myTheme ->
                { myTheme
                    | page =
                        \attrs -> myTheme.page (bgColor Element.PravdomilUi.Theme.Basic.style.back100 :: attrs)
                    , heading3 =
                        \attrs -> myTheme.heading3 (fontSize 20 :: attrs)
                }
           )
