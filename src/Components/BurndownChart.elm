module Components.BurndownChart exposing
    ( CalculateStoryPointsParams
    , InterceptMsg(..)
    , Model
    , Msg
    , ResolvedParams
    , calculatedStoryPoints
    , calculatingStoryPoints
    , init
    , onIntercept
    , update
    , view
    )

import BurndownChart
import Element.PravdomilUi exposing (..)
import ElmInterop.Ports exposing (Msg(..))
import ElmUtils.FormValidation as FormValidation
import Form exposing (Form)
import Form.View exposing (Model, State(..))
import Shell exposing (theme)
import Time exposing (Month(..))



-- MODEL


type alias Model =
    { state : State
    , chartOptionsFormModel : Form.View.Model ChartOptionsFormModel
    , displayChartOptionsForm : Bool
    , remainingStoryPoints : List Int
    , sprintProgress : Int
    }


type State
    = Undefined String
    | Ready ResolvedParams
    | CalculatingStoryPoints CalculateStoryPointsParams


type alias ResolvedParams =
    { requestedSprints : Int
    , requestedStoryPoints : Int
    , capacityPerSprint : Int
    , calculatedStoryPoints : List Int
    }


type alias CalculateStoryPointsParams =
    { requestedSprints : Int
    , capacityPerSprint : Int
    , requestedStoryPoints : Int
    }


type Msg
    = Intercept InterceptMsg
    | ChartOptionsFormSubmit (Form.View.Model ChartOptionsFormModel)
    | ProgressSprint Int


type InterceptMsg
    = CalculateStoryPoints CalculateStoryPointsParams


type alias ChartOptionsFormModel =
    { requestedSprints : String
    , capacityPerSprint : String
    , requestedStoryPoints : String
    }



-- INIT


init : Bool -> ResolvedParams -> Model
init displayBurndownChartOptionsForm params =
    let
        state =
            Ready params

        chartOptionsFormModel =
            Form.View.idle
                { requestedSprints = String.fromInt params.requestedSprints
                , capacityPerSprint = String.fromInt params.capacityPerSprint
                , requestedStoryPoints = String.fromInt params.requestedStoryPoints
                }

        remainingStoryPoints =
            [ params.requestedStoryPoints ]

        sprintProgress =
            0
    in
    Model
        state
        chartOptionsFormModel
        displayBurndownChartOptionsForm
        remainingStoryPoints
        sprintProgress



-- UPDATE


calculatingStoryPoints : CalculateStoryPointsParams -> Model -> Model
calculatingStoryPoints params model =
    let
        chartOptionsFormModel =
            model.chartOptionsFormModel
    in
    { model
        | state = CalculatingStoryPoints params
        , chartOptionsFormModel =
            { chartOptionsFormModel
                | state = Loading
            }
    }


calculatedStoryPoints : Result String (List Int) -> Model -> Model
calculatedStoryPoints storyPoints model =
    let
        chartOptionsFormModel =
            model.chartOptionsFormModel
    in
    case ( model.state, storyPoints ) of
        ( CalculatingStoryPoints params, Ok okStoryPoints ) ->
            { model
                | state =
                    Ready
                        { requestedSprints = params.requestedSprints
                        , requestedStoryPoints = params.requestedStoryPoints
                        , capacityPerSprint = params.capacityPerSprint
                        , calculatedStoryPoints = okStoryPoints
                        }
                , chartOptionsFormModel =
                    { chartOptionsFormModel
                        | state = Idle
                    }
                , remainingStoryPoints = [ params.requestedStoryPoints ]
                , sprintProgress = 0
            }

        ( CalculatingStoryPoints _, Err reason ) ->
            { model
                | state =
                    Undefined reason
                , chartOptionsFormModel =
                    { chartOptionsFormModel
                        | state = Error reason
                    }
                , remainingStoryPoints = []
                , sprintProgress = 0
            }

        _ ->
            model


update : Msg -> Model -> Model
update msg model =
    case msg of
        Intercept _ ->
            model

        ChartOptionsFormSubmit form ->
            { model | chartOptionsFormModel = form }

        ProgressSprint step ->
            progressSprint step model


minSprintProgress : Int
minSprintProgress =
    0


maxSprintProgress : ResolvedParams -> Int
maxSprintProgress params =
    List.length params.calculatedStoryPoints


progressSprint : Int -> Model -> Model
progressSprint step model =
    case model.state of
        Ready params ->
            let
                newSprintProgress =
                    model.sprintProgress + step

                offBounds =
                    newSprintProgress < minSprintProgress || newSprintProgress > maxSprintProgress params
            in
            if offBounds then
                model

            else
                let
                    remainingStoryPoints =
                        params.calculatedStoryPoints
                            |> List.take newSprintProgress
                            |> List.foldl
                                (\sp ( remaining, acc ) ->
                                    let
                                        newRemaining =
                                            remaining - sp
                                    in
                                    ( newRemaining, newRemaining :: acc )
                                )
                                ( params.requestedStoryPoints, [ params.requestedStoryPoints ] )
                            |> (\( _, acc ) -> List.reverse acc)

                    newModel =
                        { model
                            | remainingStoryPoints = remainingStoryPoints
                            , sprintProgress = newSprintProgress
                        }
                in
                newModel

        _ ->
            model


onIntercept : (Msg -> msg) -> (InterceptMsg -> msg) -> Msg -> msg
onIntercept otherwise map msg =
    case msg of
        Intercept interceptMsg ->
            map interceptMsg

        _ ->
            otherwise msg



-- VIEW


view : Model -> Element Msg
view model =
    let
        chartOptions =
            if model.displayChartOptionsForm then
                [ viewChartOptionsForm model
                , horizontalLine theme [] []
                ]

            else
                [ none ]
    in
    column []
        [ viewChart model
        , column [] (chartOptions ++ [ viewSprintStepBtns model ])
        ]


viewChart : Model -> Element msg
viewChart model =
    BurndownChart.view (chartConfig model) |> html


chartConfig : Model -> BurndownChart.Config
chartConfig model =
    let
        startDate =
            ( 2024, Jan, 8 )

        targetDate =
            ( 2024, Jan, 27 )
    in
    { name = "Project"
    , color = Nothing
    , startDate = startDate
    , baseline =
        ( startDate
        , BurndownChart.targetDate targetDate
        )
    , milestones =
        [ ( "S1", 0, Just ( 2024, Jan, 15 ) )
        , ( "S2", 0, Just ( 2024, Jan, 22 ) )
        , ( "S3", 0, Just ( 2024, Jan, 29 ) )
        ]
    , pointsRemaining = model.remainingStoryPoints
    }


viewChartOptionsForm : Model -> Element Msg
viewChartOptionsForm model =
    Form.View.asHtml
        { onChange = ChartOptionsFormSubmit
        , action = "Apply"
        , loading = "Thinking..."
        , validation = Form.View.ValidateOnSubmit
        }
        chartOptionsForm
        model.chartOptionsFormModel
        |> html


chartOptionsForm : Form ChartOptionsFormModel Msg
chartOptionsForm =
    let
        {-
           requestedSprintsField =
               Form.numberField
                   { parser = FormValidation.apply [ FormValidation.int ]
                   , value = .requestedSprints
                   , update = \value values -> { values | requestedSprints = value }
                   , error = always Nothing
                   , attributes =
                       { label = "Sprints"
                       , placeholder = "Sprints"
                       , step = Just 1
                       , min = Just 3
                       , max = Just 3
                       }
                   }
        -}
        capacityPerSprintField =
            Form.numberField
                { parser = FormValidation.apply [ FormValidation.int ]
                , value = .capacityPerSprint
                , update = \value values -> { values | capacityPerSprint = value }
                , error = always Nothing
                , attributes =
                    { label = "Capacity per sprint"
                    , placeholder = "Capacity per sprint"
                    , step = Just 1
                    , min = Just 1
                    , max = Nothing
                    }
                }

        requestedStoryPointsField =
            Form.numberField
                { parser = FormValidation.apply [ FormValidation.int ]
                , value = .requestedStoryPoints
                , update = \value values -> { values | requestedStoryPoints = value }
                , error = always Nothing
                , attributes =
                    { label = "Story points"
                    , placeholder = "Story points"
                    , step = Just 1
                    , min = Just 1
                    , max = Nothing
                    }
                }
    in
    Form.succeed (CalculateStoryPointsParams 3)
        -- |> Form.append requestedSprintsField
        |> Form.append capacityPerSprintField
        |> Form.append requestedStoryPointsField
        |> Form.map (Intercept << CalculateStoryPoints)


viewSprintStepBtns : Model -> Element Msg
viewSprintStepBtns _ =
    row []
        [ button theme
            []
            { label = text "Prev"
            , active = False
            , onPress = Just (ProgressSprint -1)
            }
        , button theme
            []
            { label = text "Next"
            , active = False
            , onPress = Just (ProgressSprint 1)
            }
        ]
