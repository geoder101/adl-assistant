/* TODO:
     Probably move all the bellow declarations out to a separate npm package and publish it.
     Sth like 'geoder101/elm-js-interop'?
     To include 'elm-ports.ts' as well.
*/

// FIXME: consider Elm's `Browser.document` vs `Browser.application` slight type diffs.

export type ElmInstance<Main, Prop extends string = 'Main'> =
  Main extends ElmMain<infer _> ? Readonly<Record<Prop, Main>> : never;

export type ElmMain<App extends ElmApp = ElmApp> = {
  readonly init: (args: ElmArgs) => App;
};

export type ElmArgs = {
  node: Element | null;
  flags?: any;
};

export type ElmApp = {};

export type ElmAppWithPorts<ElmPorts> =
  ElmApp & {
    readonly ports: ElmPorts; //ValidElmPorts<ElmPorts>;
  };

// FIXME: Ideally 'ElmPorts' would be constrained, but I cannot figure out how :/

// type ValidElmPorts<T> = T extends ElmPortIn<infer _> | ElmPortOut<infer _> ? T : never;
// type ValidElmPorts<T> = (T extends ElmPortIn<infer _> ? T : never) | (T extends ElmPortOut<infer _> ? T : never);
// type ValidElmPorts<T> =
//   T extends ElmPortIn<infer _> & ElmPortOut<infer _>
//     ? T
//     : T extends ElmPortIn<infer _>
//       ? T
//       : T extends ElmPortOut<infer _>
//         ? T
//         : never;

export type ElmPortInHandler = (msg: unknown) => void;

export type ElmPortInProp = {
  readonly subscribe: (handler: ElmPortInHandler) => void;
  readonly unsubscribe: (handler: ElmPortInHandler) => void;
};

export type ElmPortIn<Prop extends string> =
  Readonly<Record<Prop, ElmPortInProp>>;

export type ElmPortOutProp = {
  readonly send: (msg: any) => void;
};

export type ElmPortOut<Prop extends string> =
  Readonly<Record<Prop, ElmPortOutProp>>;

export type ElmPortInOut<InProp extends string, OutProp extends string> =
  ElmPortIn<InProp> & ElmPortOut<OutProp>;
