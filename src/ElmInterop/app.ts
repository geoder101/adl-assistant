import type {
  ElmInstance,
  ElmMain,
  ElmAppWithPorts,
  ElmPortInOut
} from './elm';

export type ThisElmAppPortInOut<Prop extends string> =
  ElmPortInOut<Prop, `${Prop}Answer`>;

export type ThisElmInstance = ElmInstance<ThisElmMain>;

export type ThisElmMain = ElmMain<ThisElmApp>;

export type ThisElmApp =
  ElmAppWithPorts<
    ThisElmAppPortInOut<'calculateStoryPoints'>>;

export type PortMsgCorrelationId = {
  readonly $correlationId: string;
};
