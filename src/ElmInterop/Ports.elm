module ElmInterop.Ports exposing (..)

import ElmInterop.StoryPoints as StoryPoints


type Msg
    = StoryPoints StoryPoints.Msg
