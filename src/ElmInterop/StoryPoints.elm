module ElmInterop.StoryPoints exposing
    ( CalculateReq
    , CalculateRes(..)
    , Msg(..)
    , decodeRes
    , encodeReq
    )

import Json.Decode as D
import Json.Encode as E


type Msg
    = Calculate ( String, CalculateReq )
    | CalculateAnswer (Result D.Error ( String, CalculateRes ))


type alias CalculateReq =
    { requestedSprints : Int
    , requestedStoryPoints : Int
    , capacityPerSprint : Int
    }


type CalculateRes
    = Inconclusive
    | Exact (List Int)
    | Underflow (List Int)
    | Overflow (List Int)


encodeReq : ( String, CalculateReq ) -> E.Value
encodeReq ( correlationId, req ) =
    E.object
        [ ( "$correlationId", E.string correlationId )
        , ( "requestedSprints", E.int req.requestedSprints )
        , ( "requestedStoryPoints", E.int req.requestedStoryPoints )
        , ( "constCapacityPerSprint", E.int req.capacityPerSprint )
        ]


decodeRes : D.Value -> Result D.Error ( String, CalculateRes )
decodeRes =
    D.decodeValue decoderOfRes


decoderOfRes : D.Decoder ( String, CalculateRes )
decoderOfRes =
    D.field "kind" D.string
        |> D.andThen
            (\kind ->
                let
                    decoderOfStoryPoints =
                        D.field "storyPoints" (D.list D.int)
                in
                D.map2 Tuple.pair
                    (D.field "$correlationId" D.string)
                    (case kind of
                        "inconclusive" ->
                            D.succeed Inconclusive

                        "exact" ->
                            D.map Exact decoderOfStoryPoints

                        "underflow" ->
                            D.map Underflow decoderOfStoryPoints

                        "overflow" ->
                            D.map Overflow decoderOfStoryPoints

                        _ ->
                            D.fail "Invalid 'CalculateRes' kind."
                    )
            )
