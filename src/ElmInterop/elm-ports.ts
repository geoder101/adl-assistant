import type { ElmPortInProp, ElmPortOutProp } from './elm';
import valueOrDefault from '../ts-utils/valueOrDefault';

/**
 * Guards against undefined Elm ports.
 * @param port The Elm port to guard against.
 * @returns An Elm port with NoOp defaults, if undefined, otherwise the provided port as-is.
 * @see "Ports can be dead code eliminated" note in {@link https://elm-lang.org/0.19.1/ports | Elm Ports} documentation.
 */
export function port<Port extends ElmPortInProp | ElmPortOutProp>(port: Port | undefined): Port {
  return valueOrDefault(port, noopPort as Port);
}

const noopPort: ElmPortInProp & ElmPortOutProp = {
  subscribe: noopFn,
  unsubscribe: noopFn,
  send: noopFn
};

function noopFn() {
}

export type ElmPortInMsgReader<T> = (msg: unknown) => T;

export type ElmPortInOutHandler<In, Out> = (msg: In) => Out | PromiseLike<Out>;

export function readPortMsgAsTypeCastBecauseYolo<T>(msg: unknown): T {
  return msg as T; // Trust me, I'm an engineer ;)
}

export function pipe<PortIn extends ElmPortInProp, PortOut extends ElmPortOutProp, MsgIn, MsgOut>(
  portIn: PortIn | undefined,
  portOut: PortOut | undefined,
  handler: ElmPortInOutHandler<MsgIn, MsgOut>,
  reader: ElmPortInMsgReader<MsgIn> = readPortMsgAsTypeCastBecauseYolo): void {
  const safePortIn = port(portIn);
  const safePortOut = port(portOut);
  safePortIn.subscribe(async function (msg) {
    const req = reader(msg);
    const res = await handler(req);
    safePortOut.send(res);
  });
}
