module ElmUtils.FormValidation exposing
    ( apply
    , int
    )

import Basics.Extra exposing (flip)
import Form.Decoder as Decoder exposing (Decoder)
import Result.Extra as ResultE


combineDecoderErrors : List (Result (List String) ok) -> Result String ok
combineDecoderErrors =
    List.map (Result.mapError (String.join " ")) >> List.foldl (flip ResultE.or) (Err "")


apply : List (Decoder String String ok) -> String -> Result String ok
apply decoders input =
    decoders
        |> List.map (Decoder.run >> (\f -> f input))
        |> combineDecoderErrors


int : Decoder String String Int
int =
    Decoder.int "invalid number"
