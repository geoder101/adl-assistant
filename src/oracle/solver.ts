import { Result } from '@badrap/result';
import { init } from 'z3-solver';
import type {
  Context as Z3Ctx,
  Model as Z3Model,
  Bool,
  Arith
} from 'z3-solver';
import { ResultError } from '../ts-utils/result-error';
import { toArrayAsync } from '../ts-utils/array';

// TODO: migrate to its own npm package.

export type Z3 = Z3Ctx<'main'>;

export type {
  Arith,
  IntNum,
  Bool
} from 'z3-solver';

export async function MkZ3(): Promise<Z3> {
  const { Context: Z3Ctx } = await init();
  return Z3Ctx('main');
}

export type SolveAnswer<Outcome> = Result<Outcome, OracleOutcomeError>;

export async function trySolve<Outcome>(z3: Z3, model: OracleModel<Outcome>): Promise<SolveAnswer<Outcome>> {
  try {
    const assertions = await toArrayAsync(model);
    const answer = await z3.solve(...assertions);

    let outcome: SolveAnswer<Outcome>;

    if (z3.isModel(answer)) {
      // console.debug(answer.sexpr());
      outcome = (await model.resolve(answer)).map(ok => ok, err => new OracleOutcomeError(err));
    }
    else {
      outcome = Result.err(new OracleOutcomeError(answer));
    }

    return outcome;
  } catch (err) {
    return Result.err(new OracleOutcomeError(err instanceof Error ? err : 'unknown'));
  }
}

export async function solve<Outcome>(z3: Z3, model: OracleModel<Outcome>): Promise<Outcome | undefined> {
  return await unwrapOutcome(trySolve(z3, model));
}

export async function unwrapOutcome<Outcome>(answer: SolveAnswer<Outcome> | PromiseLike<SolveAnswer<Outcome>>): Promise<Outcome | undefined> {
  const awaitedAnswer = await answer;
  return awaitedAnswer.isOk ? awaitedAnswer.unwrap() : undefined;
}

export class OracleOutcomeError extends ResultError<Error | 'unsat' | 'unknown'> {
}

export type OracleEvaluatedModel = Z3Model;

export interface OracleModelInstance extends AsyncIterable<Bool> {
}

export class OracleModelOutcomeEvaluator {
  constructor(protected readonly z3: Z3) {
  }

  public int(model: OracleEvaluatedModel, expr: Arith): Result<number> {
    const evalExpr = model.get(expr);
    return this.z3.isIntVal(evalExpr)
      ? Result.ok(Number(evalExpr.value()))
      : Result.err(new Error(`'${expr}' is not an integer.`));
  }
}

export abstract class OracleModel<Outcome> implements OracleModelInstance {
  private readonly assertions: Bool[] = [];

  private readonly attachedModels: OracleModelInstance[] = [];

  protected eval: OracleModelOutcomeEvaluator;

  protected constructor(protected readonly z3: Z3, evaluator?: OracleModelOutcomeEvaluator) {
    this.eval = evaluator ?? new OracleModelOutcomeEvaluator(z3);
  }

  private static add<T>(arr: T[], items: T[]) {
    arr.push(...items);
  }

  private static remove<T>(arr: T[], items: T[]) {
    for (const item of items) {
      const idx = arr.indexOf(item);
      if (idx !== -1) {
        arr.splice(idx, 1);
      }
    }
  }

  private static reset<T>(arr: T[]) {
    arr.length = 0;
  }

  protected assert(...exprs: Bool[]) {
    OracleModel.add(this.assertions, exprs);
  }

  protected reject(...exprs: Bool[]) {
    OracleModel.remove(this.assertions, exprs);
  }

  protected resetAssertions() {
    OracleModel.reset(this.assertions);
  }

  protected attach(...models: OracleModelInstance[]) {
    OracleModel.add(this.attachedModels, models);
  }

  protected detach(...models: OracleModelInstance[]) {
    OracleModel.remove(this.attachedModels, models);
  }

  protected resetAttachedModels() {
    OracleModel.reset(this.attachedModels);
  }

  protected abstract realize(): void | PromiseLike<void>;

  public abstract resolve(model: OracleEvaluatedModel): Result<Outcome> | PromiseLike<Result<Outcome>>;

  protected reset() {
    this.resetAssertions();
    this.resetAttachedModels();
  }

  async *[Symbol.asyncIterator](): AsyncIterator<Bool> {
    this.reset();
    await this.realize();
    for (const model of this.attachedModels) {
      yield* model;
    }
    yield* this.assertions;
  }
}
