import { curry2 } from 'ts-curry';
import valueOrDefault from '../ts-utils/valueOrDefault';
import type { ThisElmApp, PortMsgCorrelationId } from '../ElmInterop/app';
import { pipe, type ElmPortInOutHandler } from '../ElmInterop/elm-ports';
import { solve, type Z3, } from './solver';
import {
  workloadModelConfigurationDefaults,
  constNumOfSubTasksPerTask,
  WorkloadModel,
  type WorkloadModelConfiguration
} from './models/workload';

export type CalculateWorkloadReq = PortMsgCorrelationId & {
  readonly requestedSprints: number;
  readonly requestedStoryPoints: number;
  readonly daysPerSprint?: number;
  readonly capacityPerSprint?: Map<number, number>;
  readonly constCapacityPerSprint?: number;
  readonly sprintUpperTolerance?: number;
};

export type CalculateWorkloadResStoryPoints = {
  readonly storyPoints: number[];
};

export type CalculateWorkloadRes =
  | { readonly kind: 'inconclusive'; } & PortMsgCorrelationId
  | { readonly kind: 'exact'; } & CalculateWorkloadResStoryPoints & PortMsgCorrelationId
  | { readonly kind: 'underflow'; } & CalculateWorkloadResStoryPoints & PortMsgCorrelationId
  | { readonly kind: 'overflow'; } & CalculateWorkloadResStoryPoints & PortMsgCorrelationId;

type CalculateWorkloadHandler = ElmPortInOutHandler<CalculateWorkloadReq, CalculateWorkloadRes>;

export async function init(z3: Z3, app: ThisElmApp) {
  const handler: CalculateWorkloadHandler = curry2(calculateWorkload)(z3);
  pipe(
    app.ports.calculateStoryPoints,
    app.ports.calculateStoryPointsAnswer,
    handler);
}

export const calculateWorkloadDefaults = {
  sprintUpperTolerance: 5 // Arbitrary value; no specific underlying reasoning.
} as const;

export async function calculateWorkload(z3: Z3, req: CalculateWorkloadReq): Promise<CalculateWorkloadRes> {
  const $correlationId = req.$correlationId;
  let res: CalculateWorkloadRes = { $correlationId, kind: 'inconclusive' };

  const modelConfig: WorkloadModelConfiguration = {
    totalStoryPoints: req.requestedStoryPoints,
    numOfNormalPeriods: 0,
    numOfExtraPeriods: 1,
    numOfTasksPerPeriod:
      valueOrDefault(
        req.daysPerSprint,
        workloadModelConfigurationDefaults.numOfTasksPerPeriod),
    numOfSubTasksPerTask:
      req.capacityPerSprint
        ? { kind: 'per period', num: req.capacityPerSprint }
        : req.constCapacityPerSprint
          ? constNumOfSubTasksPerTask(req.constCapacityPerSprint)
          : undefined
  };

  // In order to find overflows.
  const expectedNumOfPeriodsUpperTolerance =
    valueOrDefault(
      req.sprintUpperTolerance,
      calculateWorkloadDefaults.sprintUpperTolerance);

  const expectedNumOfPeriods =
    req.requestedSprints + expectedNumOfPeriodsUpperTolerance;

  const expectedStoryPointsCardinality =
    req.requestedSprints * modelConfig.numOfTasksPerPeriod!;

  /* NOTE:
  *   We could have set the extra periods be the upper tolerance and be done with it,
  *   but we need to go iteratively so as to be able to also find underflows.
  */

  for (let periodNo = 0; periodNo < expectedNumOfPeriods; periodNo++) {
    modelConfig.numOfNormalPeriods = periodNo;

    const model = new WorkloadModel(z3, modelConfig);
    const storyPoints = await solve(z3, model);

    if (storyPoints === undefined) {
      // Keep "ascending" to an "optimum".
      // TODO: Can we incrementally re-use past facts (by _pushing_ solver states)?
      continue;
    }

    const actualStoryPointsCardinality = storyPoints.length;
    const storyPointsCardinalityDelta = actualStoryPointsCardinality - expectedStoryPointsCardinality;

    if (storyPointsCardinalityDelta === 0) {
      res = { $correlationId, kind: 'exact', storyPoints };
    }
    else if (storyPointsCardinalityDelta < 0) {
      res = { $correlationId, kind: 'underflow', storyPoints };
    }
    else if (storyPointsCardinalityDelta > 0) {
      res = { $correlationId, kind: 'overflow', storyPoints };
    }

    break;
  }

  console.debug(JSON.stringify(res));

  return res;
}
