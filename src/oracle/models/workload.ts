import { Result } from '@badrap/result';
import valueOrDefault from '../../ts-utils/valueOrDefault';
import {
  type Z3,
  type Arith,
  type OracleEvaluatedModel,
  OracleModel
} from '../solver';

export type WorkloadModelConfiguration = {
  totalStoryPoints: number;
  numOfNormalPeriods: number;
  numOfExtraPeriods?: number;
  numOfTasksPerPeriod?: number;
  numOfSubTasksPerTask?: NumOfSubTasksPerTask;
  availableStoryPoints?: number[];
};

export type NumOfSubTasksPerTask =
  | { kind: 'for all periods', num: number; }
  | { kind: 'per period', num: Map<number, number>; };

export function constNumOfSubTasksPerTask(num: number) {
  return { kind: 'for all periods', num } satisfies NumOfSubTasksPerTask;
}

export const workloadModelConfigurationDefaults = {
  numOfExtraPeriods: 0,
  numOfTasksPerPeriod: 5,
  numOfSubTasksPerTask: constNumOfSubTasksPerTask(1),
  availableStoryPoints: [0, 1, 2, 3, 5, 8, 13] as readonly number[]
} as const;

export class WorkloadModel extends OracleModel<number[]> {
  readonly subModels: {
    readonly normalPeriods: PeriodModel[];
    readonly extraPeriods: PeriodModel[];
    readonly allPeriods: PeriodModel[];
  };

  constructor(
    z3: Z3,
    private readonly config: WorkloadModelConfiguration) {
    super(z3);

    const numOfNormalPeriods = this.config.numOfNormalPeriods;

    const numOfExtraPeriods =
      valueOrDefault(
        this.config.numOfExtraPeriods,
        workloadModelConfigurationDefaults.numOfExtraPeriods);

    const normalPeriods =
      Array.from(
        { length: numOfNormalPeriods },
        (_, index) => new PeriodModel(this.z3, 'normal', index, this.config));

    const extraPeriods =
      Array.from(
        { length: numOfExtraPeriods },
        (_, index) => new PeriodModel(this.z3, 'extra', numOfNormalPeriods + index, this.config));

    const allPeriods = [...normalPeriods, ...extraPeriods];

    this.subModels = {
      normalPeriods,
      extraPeriods,
      allPeriods
    };
  }

  protected override realize(): void {
    this.attach(...this.subModels.allPeriods);

    const allPeriodStoryPoints =
      this.subModels.allPeriods
        .flatMap(period => period.subModels.tasks)
        .map(task => task.variables.storyPoints);

    const totalStoryPoints = this.z3.Sum(this.z3.Int.val(0), ...allPeriodStoryPoints);
    this.assert(totalStoryPoints.eq(this.config.totalStoryPoints));
  }

  public override resolve(model: OracleEvaluatedModel): Result<number[]> {
    return Result.ok(this.subModels.allPeriods.flatMap(period => period.resolve(model).unwrap()));
  }
}

class PeriodModel extends OracleModel<number[]> {
  readonly subModels: {
    readonly tasks: TaskModel[];
  };

  constructor(
    z3: Z3,
    private readonly type: 'normal' | 'extra',
    private readonly index: number,
    private readonly config: WorkloadModelConfiguration) {
    super(z3);

    const numOfTasks =
      valueOrDefault(
        this.config.numOfTasksPerPeriod,
        workloadModelConfigurationDefaults.numOfTasksPerPeriod);

    this.subModels = {
      tasks: Array.from({ length: numOfTasks }, () => new TaskModel(this.z3, this.index, this.config))
    };
  }

  protected override realize(): void {
    this.attach(...this.subModels.tasks);

    const allTaskStoryPoints = this.subModels.tasks.map(task => task.variables.storyPoints);

    for (const taskStoryPoints of allTaskStoryPoints) {
      switch (this.type) {
        case 'normal':
          this.assert(taskStoryPoints.ge(1));
          break;
        case 'extra':
          this.assert(taskStoryPoints.ge(0));
          break;
      }
    }
  }

  public override resolve(model: OracleEvaluatedModel): Result<number[]> {
    return Result.ok(
      this
        .subModels
        .tasks
        .flatMap(task => task.resolve(model).unwrap())
        .filter(sp => sp > 0));
  }
}

class TaskModel extends OracleModel<number> {
  readonly variables: {
    readonly storyPoints: Arith;
  };

  readonly subModels: {
    readonly subTasks: SubTaskModel[];
  };

  constructor(
    z3: Z3,
    private readonly periodIndex: number,
    private readonly config: WorkloadModelConfiguration) {
    super(z3);

    this.variables = {
      storyPoints: this.z3.Int.fresh()
    };

    const numOfSubTasksDU = this.config.numOfSubTasksPerTask;
    let numOfSubTasks = workloadModelConfigurationDefaults.numOfSubTasksPerTask.num;

    if (numOfSubTasksDU) {
      if (numOfSubTasksDU.kind === 'for all periods') {
        numOfSubTasks = numOfSubTasksDU.num;
      }
      else if (numOfSubTasksDU.kind === 'per period') {
        numOfSubTasks =
          valueOrDefault(
            numOfSubTasksDU.num.get(this.periodIndex),
            workloadModelConfigurationDefaults.numOfSubTasksPerTask.num);
      }
    }

    this.subModels = {
      subTasks: Array.from({ length: numOfSubTasks }, () => new SubTaskModel(this.z3, this.config))
    };
  }

  protected override realize(): void {
    this.attach(...this.subModels.subTasks);

    const allSubTaskStoryPoints =
      this.subModels.subTasks
        .map(task => task.variables.storyPoints);

    const totalStoryPoints = this.z3.Sum(this.z3.Int.val(0), ...allSubTaskStoryPoints);
    this.assert(totalStoryPoints.eq(this.variables.storyPoints));
  }

  public override resolve(model: OracleEvaluatedModel): Result<number> {
    return this.eval.int(model, this.variables.storyPoints);
  }
}

class SubTaskModel extends OracleModel<number> {
  readonly variables: {
    readonly storyPoints: Arith;
  };

  constructor(
    z3: Z3,
    private readonly config: WorkloadModelConfiguration) {
    super(z3);

    this.variables = {
      storyPoints: this.z3.Int.fresh()
    };
  }

  protected override realize(): void {
    const availableStoryPoints =
      valueOrDefault(
        this.config.availableStoryPoints,
        workloadModelConfigurationDefaults.availableStoryPoints);

    this.assert(this.z3.Or(...availableStoryPoints.map(sp => this.variables.storyPoints.eq(sp))));
  }

  public override resolve(model: OracleEvaluatedModel): Result<number> {
    return this.eval.int(model, this.variables.storyPoints);
  }
}

/*

The general idea of the model above boils down to this prototype:

  ; Given sprints 1-3 plus 1 extra.

  (declare-const s1 Int)
  (declare-const s2 Int)
  (declare-const s3 Int)
  (declare-const s4 Int)

  ; And each sprint can take up 0-13 story points.

  (assert (or (= s1 0) (= s1 1) (= s1 2) (= s1 3) (= s1 5) (= s1 8) (= s1 13)))
  (assert (or (= s2 0) (= s2 1) (= s2 2) (= s2 3) (= s2 5) (= s2 8) (= s2 13)))
  (assert (or (= s3 0) (= s3 1) (= s3 2) (= s3 3) (= s3 5) (= s3 8) (= s3 13)))
  (assert (or (= s4 0) (= s4 1) (= s4 2) (= s4 3) (= s4 5) (= s4 8) (= s4 13)))

  ; And sprints 1-3 *must* have story points.
  ; And (extra) sprint 4 *may* have story points, in case of overflow.

  ; How many story points can each sprint take,
  ; with respect to an upper limit?

  (declare-const story-points-normal Int)
  (declare-const story-points-overflow Int)

  (assert (= story-points-normal 28))
  (assert (= story-points-overflow (+ story-points-normal 2)))

  (push)

  ;; Example 1

  (assert (< 0 s1))
  (assert (< 0 s2))
  (assert (< 0 s3))
  (assert (= 0 s4))

  (assert (= (+ s1 s2 s3 s4) story-points-normal))

  (echo "
  Example 1: no overflow with no extra period
  ")

  (check-sat)
  (get-model)

  (pop)
  (push)

  ;; Example 2

  (assert (< 0 s1))
  (assert (< 0 s2))
  (assert (< 0 s3))
  (assert (= 0 s4))

  (assert (= (+ s1 s2 s3 s4) story-points-overflow))

  (echo "
  Example 2: overflow with no extra period
  ")

  (check-sat)

  (pop)
  (push)

  ;; Example 3

  (assert (< 0 s1))
  (assert (< 0 s2))
  (assert (< 0 s3))
  (assert (<= 0 s4))

  (assert (= (+ s1 s2 s3 s4) story-points-overflow))

  (echo "
  Example 3: overflow with extra period
  ")

  (check-sat)
  (get-model)

Output:

  Example 1: no overflow with no extra period

  sat
  (
    (define-fun s3 () Int
      13)
    (define-fun s1 () Int
      2)
    (define-fun s4 () Int
      0)
    (define-fun story-points-normal () Int
      28)
    (define-fun s2 () Int
      13)
    (define-fun story-points-overflow () Int
      30)
  )

  Example 2: overflow with no extra period

  unsat

  Example 3: overflow with extra period

  sat
  (
    (define-fun s3 () Int
      13)
    (define-fun s1 () Int
      1)
    (define-fun s4 () Int
      8)
    (define-fun story-points-normal () Int
      28)
    (define-fun s2 () Int
      8)
    (define-fun story-points-overflow () Int
      30)
  )

So, on 30 story points in total we need to allocate one more sprint,
that is sprint #4, which will take up 8 story points.

You can try this online at <https://microsoft.github.io/z3guide/playground/Freeform%20Editing/>

*/
