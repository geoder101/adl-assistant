// @ts-ignore
import { Elm } from './Main.elm';
import { type ThisElmInstance } from './ElmInterop/app';
import { MkZ3, type Z3, } from './oracle/solver';
import * as SprintOracle from './oracle/sprint';

console.log('Loading...');

const appElem = document.getElementById('app');

let stillLoading: number | null =
  appElem === null
    ? null
    : window.setTimeout(() => {
      if (appElem.textContent === 'Loading...') {
        appElem.textContent = 'Loading... In the meantime, how is your day going?';
      }
      stillLoading = null;
    }, 5000);

(async () => {
  const z3 = await MkZ3();
  // await solverSanityCheck(z3);

  const elm: ThisElmInstance = Elm;
  const app = elm.Main.init({ node: appElem });
  await SprintOracle.init(z3, app);

  if (stillLoading !== null) {
    clearTimeout(stillLoading);
    stillLoading = null;
  }

  console.log('Loading... DONE');
})();

// @ts-ignore
async function solverSanityCheck(z3: Z3) {
  const x = z3.Int.const('x');
  const answer = await z3.solve(x.eq(42));
  if (z3.isModel(answer)) {
    console.log(`Z3 found a solution: ${answer.get(x)}`);
  } else {
    console.error("Z3 didn't find a solution.");
  }
}
