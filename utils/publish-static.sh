#!/usr/bin/env bash

# Make sure cwd is the expected one.
[ ${BASH_SOURCE%/*} == '.' ] || {
  pushd $(dirname $0)
  POPD=true
}

PUBLIC_DIR=../public
STATIC_DIR=../static
NODE_MODULES_DIR=../node_modules

rm $PUBLIC_DIR/*

cp -a $STATIC_DIR/* $PUBLIC_DIR/

# See <https://www.npmjs.com/package/z3-solver#limitations>
# cp $NODE_MODULES_DIR/coi-serviceworker/coi-serviceworker.min.js $PUBLIC_DIR/coi-serviceworker.js
cp $NODE_MODULES_DIR/z3-solver/build/z3-built.js $PUBLIC_DIR/z3.js
cp $NODE_MODULES_DIR/z3-solver/build/z3-built.wasm $PUBLIC_DIR/z3-built.wasm
cp $NODE_MODULES_DIR/z3-solver/build/z3-built.worker.js $PUBLIC_DIR/z3-built.worker.js

[ $POPD ] && popd
